import { View, Text, Button ,Image, StyleSheet, ImageBackground} from 'react-native';
import React from 'react';
import { Vibration } from "react-native";

function HomeScreen({ navigation }) {
    console.log("message")
    return (
    <ImageBackground
            source={require("../fond_ecran.png")}
            style={styles.container}

        >

        
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
           
            <Text style={styles.titre}>Bienvenue sur la platforme de recherche de praticien {"\n"}</Text>
            <Image source={require('../logo.png')} 
                    style={styles.logoBruz}
            />
            <Text style={styles.textLieu}>{"\n"} Lieu : Bruz 📍 {"\n"}</Text>
            
            <Text>{"\n"}</Text>
          

            <Button 
                title="Rechercher un praticien"
                color = "#68656e"
                onPress={() => {
                    navigation.navigate('Medecin');
                    Vibration.vibrate()
                    
                }
                }
            />
            <Text> {"\n"}</Text> 
            <Button
                title="Afficher tous les praticiens selon la spécialité"
                color = "#68656e"
                onPress={() => {
                    navigation.navigate('Specialite');
                    Vibration.vibrate()
                }
                }
            />
            <Text> {"\n"}</Text>
            <Text style={styles.textBasique}>
                Ici vous trouverez les 
                <Text style={styles.gras}> horaires</Text>🕐, les 
                <Text style={styles.gras}> numéros de téléphone</Text>☎️ et les 
                <Text style={styles.gras}> adresses</Text>🏣 : {"\n"}{"\n"} médecins généralistes, sages-femmes, dentistes, podologues, kinésithérapeute{"\n"}</Text>
            
            
        </View>
        </ImageBackground>
    );
}

var styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    gras:{
        fontWeight: "bold",
    },
    logoBruz:{
        width: 40, 
        height: 40 ,
        padding: 10, 
        margin: 5,
        resizeMode: 'stretch',
    },
    titre:{
        fontSize: 24, 
        fontWeight: 'bold', 
        textAlign: "center",
    },
    textLieu:{
        fontSize: 24, 
        textAlign: "center" ,
    },
    textBasique :{
        fontSize: 14,
        textAlign: "center" 
    }

    
});


export default HomeScreen;
