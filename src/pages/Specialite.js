import React from 'react';
import practicien from "../practicien.json";
import { Alert, Modal, TouchableOpacity, Linking, Pressable, FlatList, Button,Platform, SafeAreaView,StyleSheet,View, Text, TextInput } from 'react-native';


class MedecinScreen extends React.Component { 
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      value: '',
      modalVisible: false,
    };
    this.arrayNew=practicien
  }
  
  static navigationOptions = {
		title: 'Home',
		header: null
      };
      renderSeparator = () => {
        return (
          <View
            style={{
              height: 1,
              width: '100%',
              backgroundColor: '#CED0CE',
            }}
          />
        );
      };
    
      searchItems = text => {
        let newData = this.arrayNew.filter(item => {
          const itemData = `${item.specialite.toUpperCase()}`;
          const textData = text.toUpperCase();
        if(text.length >0 ){
          return itemData.indexOf(textData) > -1;
        }
        });
        this.setState({
          data: newData,
          value: text,
        });
      };
    
      renderHeader = () => {
        return (
          <TextInput
            style={{ height: 60, borderRadius: 20, borderColor: '#000', borderWidth: 1 }}
            placeholder="  Spécialité"
            onChangeText={text => this.searchItems(text)}
            value={this.state.value}
          />
        );
      };
      setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
      };
      render() {
        const { modalVisible } = this.state;
        return (
          <View
            style={{
              flex: 1,
              padding: 25,
              width: '98%',
              alignSelf: 'center',
              justifyContent: 'center',
            }}>
            <Text>Rentre la specialité parmi la liste : {"\n"}</Text>
            <Text>Medecin, Sage-femme, Dentiste, Podologue, Kinésithérapeute {"\n"}</Text>
            <FlatList
              data={this.state.data}
              renderItem={({ item }) => (
                <View>
                  <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                      Alert.alert("Modal has been closed.");
                      this.setModalVisible(!modalVisible);
                    }}
                  >
                    <View>
                      <View style={{margin: 20, backgroundColor: "white", borderRadius: 20, padding: 35, alignItems: "center", shadowColor: "#000", shadowOffset: {width: 0, height: 2},shadowOpacity: 0.25,shadowRadius: 4, elevation: 5}}>
                        <Text style={{marginBottom: 15,textAlign: "center"}}>{item.prenom}</Text>
                        <TouchableOpacity onPress={() => Linking.openURL(item.lien_maps)}>
                          <Text style={{color: 'blue'}}>
                            Adresse maps
                          </Text>
                        </TouchableOpacity>
                        <Pressable
                          style={[{borderRadius: 20,padding: 10,elevation: 2}, {backgroundColor: "#2196F3"}]}
                          onPress={() => this.setModalVisible(!modalVisible)}
                        >
                          <Text>Quitter</Text>
                        </Pressable>
                      </View>
                    </View>
                  </Modal>
                  <Pressable
                    style={[{padding: 10,elevation: 2}, {backgroundColor: "white"}]}
                    onPress={() => this.setModalVisible(true)}
                  >
                    <Text>{item.prenom}</Text>
                  </Pressable>
                </View>
              )}
              keyExtractor={item => item.specialite}
              ItemSeparatorComponent={this.renderSeparator}
              ListHeaderComponent={this.renderHeader}
            />
          </View>
        );
      }
    }

export default MedecinScreen;
