import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './Home';
import MedecinScreen from './Medecin';
import Specialite from './Specialite';
import {Vibration} from "react-native";

const Tab = createBottomTabNavigator();

const vibratoOnTapPress = () =>({
    tabPress: () => {
        Vibration.vibrate();
        },
    }
);

const Navig = () => {
    return ( 
      <NavigationContainer>
        <Tab.Navigator>
            <Tab.Screen 
                name="Home" 
                component={HomeScreen} 
                options={{
                    tabBarLabel: 'Home'}}
                    listeners={vibratoOnTapPress}/>
            <Tab.Screen 
                name="Medecin" 
                component={MedecinScreen} 
                options={{
                    tabBarLabel: 'Médecin'}}
                    listeners={vibratoOnTapPress}
                    />
            <Tab.Screen 
                name="Specialite" 
                component={Specialite} 
                options={{
                    tabBarLabel: 'Specialite'}}
                    listeners={vibratoOnTapPress}
                    />
                   
        </Tab.Navigator>
      </NavigationContainer>
  );
};

export default Navig;