// App.js

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
// App.js

import 'react-native-gesture-handler';
import React from 'react';
import Navig from './pages/Navigation';


const App = () => {
  return (
    <Navig />
  );
}

export default App;
